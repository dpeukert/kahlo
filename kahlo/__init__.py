# PYTHON_ARGCOMPLETE_OK

__version__ = '1.5.3'

import argcomplete
import argparse
import configparser
import datetime
import http
import io
import json
import logging
import os
import re
import shutil
import signal
import stat
import sys
import time
import urllib
import xml
import yt_dlp

# Logging
class KahloConsoleLogFormatter(logging.Formatter):
	def __init__(self, fmt=None):
		self._formatters = {}
		level_map = [
			{
				'level': logging.CRITICAL,
				'display_level': True,
				'bold': True,
				'color': 31
			},
			{
				'level': logging.ERROR,
				'display_level': True,
				'bold': True,
				'color': 31
			},
			{
				'level': logging.WARNING,
				'display_level': True,
				'bold': True,
				'color': 33
			},
			{
				'level': logging.INFO,
				'display_level': False,
			},
			{
				'level': logging.DEBUG,
				'display_level': True,
				'bold': False,
				'color': 36
			},
		]

		for level_item in level_map:
			if level_item['display_level'] is True:
				format_string_prefix = '\033['

				if level_item['bold'] is True:
					format_string_prefix += '1'
				else:
					format_string_prefix += '0'

				format_string_prefix += ';{}m{}:\033[0m'.format(level_item['color'], logging.getLevelName(level_item['level']))
				self._formatters[level_item['level']] = logging.Formatter(fmt='{} {}'.format(format_string_prefix, fmt))

		super(KahloConsoleLogFormatter, self).__init__(fmt=fmt)

	def format(self, record):
		if record.levelno in self._formatters:
			return self._formatters[record.levelno].format(record)

		return super(KahloConsoleLogFormatter, self).format(record)

class KahloLogger:
	def __init__(self, verbose=True):
		self.verbose = verbose
		self.logger = logging.getLogger(__name__)
		self.logger.setLevel(logging.DEBUG)

		console_handler = logging.StreamHandler()
		console_handler.setLevel(logging.DEBUG if self.verbose else logging.INFO)
		console_handler.setFormatter(KahloConsoleLogFormatter(fmt='%(message)s'))
		self.logger.addHandler(console_handler)

	def separate_exception(self, **kwargs):
		main_kwargs = {key: value for key, value in kwargs.items() if key != 'exc_info'}
		exception_kwargs = {key: value for key, value in kwargs.items() if key == 'exc_info'}
		return main_kwargs, exception_kwargs

	def debug(self, msg, *args, **kwargs):
		main_kwargs, exception_kwargs = self.separate_exception(**kwargs)
		self.logger.debug(msg, *args, **main_kwargs)

		if len(exception_kwargs) > 0:
			self.logger.debug('', *args, **exception_kwargs)

	def info(self, msg, *args, **kwargs):
		main_kwargs, exception_kwargs = self.separate_exception(**kwargs)
		self.logger.info(msg, *args, **main_kwargs)

		if len(exception_kwargs) > 0:
			self.logger.debug('', *args, **exception_kwargs)

	def warning(self, msg, *args, **kwargs):
		main_kwargs, exception_kwargs = self.separate_exception(**kwargs)
		self.logger.warning(msg, *args, **main_kwargs)

		if len(exception_kwargs) > 0:
			self.logger.debug('', *args, **exception_kwargs)

	def error(self, msg, *args, **kwargs):
		main_kwargs, exception_kwargs = self.separate_exception(**kwargs)
		self.logger.error(msg, *args, **main_kwargs)

		if len(exception_kwargs) > 0:
			self.logger.debug('', *args, **exception_kwargs)

	def critical(self, msg, *args, **kwargs):
		main_kwargs, exception_kwargs = self.separate_exception(**kwargs)
		self.logger.critical(msg, *args, **main_kwargs)

		if len(exception_kwargs) > 0:
			self.logger.debug('', *args, **exception_kwargs)

	def log(self, level, msg, *args, **kwargs):
		main_kwargs, exception_kwargs = self.separate_exception(**kwargs)
		self.logger.log(level, msg, *args, **main_kwargs)

		if len(exception_kwargs) > 0:
			self.logger.debug('', *args, **exception_kwargs)

	def exception(self, msg, *args, **kwargs):
		main_kwargs, exception_kwargs = self.separate_exception(**kwargs)
		self.logger.exception(msg, *args, **main_kwargs)

		if len(exception_kwargs) > 0:
			self.logger.debug('', *args, **exception_kwargs)

class KahloYDLLogger:
	def __init__(self, logger):
		self.logger = logger

	def debug(self, msg, *args, **kwargs):
		self.logger.debug(msg, *args, **kwargs)

	def info(self, msg, *args, **kwargs):
		self.logger.debug(msg, *args, **kwargs)

	def warning(self, msg, *args, **kwargs):
		self.logger.debug(msg, *args, **kwargs)

	def error(self, msg, *args, **kwargs):
		self.logger.debug(msg, *args, **kwargs)

	def critical(self, msg, *args, **kwargs):
		self.logger.debug(msg, *args, **kwargs)

	def log(self, level, msg, *args, **kwargs):
		self.logger.debug(msg, *args, **kwargs)

	def exception(self, msg, *args, **kwargs):
		self.logger.debug(msg, *args, **kwargs)

# Logic
class Kahlo:
	SOURCES = {
		'youtube': {
			'readable_name': 'YouTube',
			'match_domains': ['youtube.com', 'youtu.be', 'youtube.googleapis.com'],
			'api_base_url': 'https://www.youtube.com/feeds/videos.xml?channel_id=',
			'video_base_url': 'https://www.youtube.com/watch?v='

		},
		'nebula': {
			'readable_name': 'Nebula',
			'match_domains': ['watchnebula.com', 'nebula.app', 'nebula.tv'],
			'api_base_url': 'https://content.api.nebula.app/video/',
			'video_base_url': 'https://nebula.tv/videos/'
		}
	}

	FALLBACK_SOURCE = 'youtube'

	USER_AGENT = '{}/{} (+https://gitlab.com/dpeukert/{})'.format(__name__, __version__, __name__)

	def __init__(self, logger=None):
		if logger is not None:
			self.logger = logger
		else:
			self.logger = KahloLogger()

		self.ydl_logger = KahloYDLLogger(self.logger)

	# Helper functions
	def normalize_string(self, string):
		if type(string) is not str or (string := string.strip()) == '':
			return None
		else:
			return string

	def normalize_prop_string(self, parent, prop, throw=True):
		normalized_string = None
		if type(normalized_string := self.normalize_string(parent.get(prop))) is not str:
			if throw is True:
				self.logger.error('Required value {} not present'.format(prop))
				raise RuntimeError
			else:
				return None
		else:
			return normalized_string

	def sanitize_filename(self, filename):
		# Filter out characters not allowed by ext4, exFAT and NTFS
		for character in ['"', '*', '/', ':', '<', '>', '?', '\\', '|', '\00']:
			filename = filename.replace(character, ' ')

		# Replace any occurences of more than one space with a single space
		filename = re.sub(r'( ){2,}', ' ', filename)

		# Trim any leading or trailing spaces
		filename = filename.strip()

		return filename

	def retry(self, func, *args, retry_count=0, **kwargs):
		for retry in range(1 + retry_count):
			if retry > 0:
				time_to_next_retry = pow(2, retry)
				self.logger.error('Retrying in {} second{}...'.format(time_to_next_retry, '' if time_to_next_retry == 1 else 's'))
				time.sleep(time_to_next_retry)

			try:
				return func(*args, **kwargs)
			except RuntimeError:
				pass

		if retry_count > 0:
			self.logger.error('Retried {} time{}, skipping...'.format(retry_count, '' if retry_count == 1 else 's'))

		return False

	# Config
	def get_config_dir(self):
		if self._config_dir is None:
			self._config_dir = os.path.join(
				os.environ.get('XDG_CONFIG_HOME') or
				os.environ.get('APPDATA') or
				os.path.join(os.environ.get('HOME'), '.config'),
				__name__
			)

			try:
				os.makedirs(self._config_dir, exist_ok=True)
			except OSError as error:
				self.logger.critical('Unable to create config directory {}, aborting...'.format(self._config_dir), exc_info=error)
				sys.exit(1)

		return self._config_dir

	def set_config_dir(self, value):
		self._config_dir = value

		try:
			# Create the directory if we're setting the config_dir to something else,
			# as self._config_dir won't be None by the time we call the getter
			os.makedirs(self._config_dir, exist_ok=True)
		except OSError as error:
			self.logger.critical('Unable to create config directory {}, aborting...'.format(self._config_dir), exc_info=error)
			sys.exit(1)

	_config_dir = None
	config_dir = property(get_config_dir, set_config_dir)

	def get_config(self):
		if self._config is None:
			self._config = {}

			file_config = configparser.ConfigParser(empty_lines_in_values=False)
			dummy_section = 'dummy_section'

			try:
				with open(os.path.join(self.config_dir, 'config'), 'r') as f:
					config_string = '[{}]\n'.format(dummy_section) + f.read()
					file_config.read_string(config_string)
			except FileNotFoundError:
				pass
			except OSError as error:
				self.logger.critical('Unable to read config file {}'.format(os.path.join(self.config_dir, 'config')), exc_info=error)
				sys.exit(1)

			self._config['cache_directory'] = file_config.get(dummy_section, 'cache_directory', fallback=os.path.join(
				os.environ.get('XDG_CACHE_HOME') or
				os.environ.get('LOCALAPPDATA') or
				os.path.join(os.environ.get('HOME'), '.cache'),
				__name__
			))

			self._config['video_directory'] = file_config.get(dummy_section, 'video_directory', fallback=os.path.join(os.environ['HOME'], __name__))

			self._config['retries'] = 5

			try:
				retries = file_config.getint(dummy_section, 'retries', fallback=5)
				if retries >= 0:
					self._config['retries'] = retries
			except ValueError:
				pass

			self._config['channels_per_run'] = None
			channels_per_run = file_config.get(dummy_section, 'channels_per_run', fallback=None)

			if type(channels_per_run) is str:
				try:
					channels_per_run = int(channels_per_run)
					if channels_per_run > 0:
						self._config['channels_per_run'] = channels_per_run
				except ValueError:
					pass

			self._config['ignore_videos'] = []
			ignore_videos = file_config.get(dummy_section, 'ignore_videos', fallback=None)

			if type(ignore_videos) is str:
				ignore_videos = ignore_videos.splitlines()

				for regex in ignore_videos:
					regex = regex.strip()

					if regex != '':
						self._config['ignore_videos'].append(re.compile(regex, re.IGNORECASE))

			self._config['max_resolution'] = None

			try:
				max_resolution = file_config.getint(dummy_section, 'max_resolution', fallback=None)
				if max_resolution is not None and max_resolution > 0:
					self._config['max_resolution'] = max_resolution
			except ValueError:
				pass

			for prop in ['description', 'subtitles']:
				try:
					self._config[prop] = file_config.getboolean(dummy_section, prop, fallback=True)
				except ValueError:
					self._config[prop] = True

		return self._config

	_config = None
	config = property(get_config)

	def get_video_dir(self):
		if self._video_dir is None:
			self._video_dir = self.config['video_directory']

			if os.path.isabs(self._video_dir) is False:
				self.logger.critical('Unable to create video directory, as {} is not an absolute path, aborting...'.format(self._video_dir))
				sys.exit(1)

			try:
				os.makedirs(self._video_dir, exist_ok=True)
			except OSError as error:
				self.logger.critical('Unable to create video directory {}, aborting...'.format(self._video_dir), exc_info=error)
				sys.exit(1)

		return self._video_dir

	_video_dir = None
	video_dir = property(get_video_dir)

	def get_channels(self):
		if self._channels is None:
			self._channels = {}

			try:
				with open(os.path.join(self.config_dir, 'channels'), 'r') as f:
					for line in f:
						line = line.split('#', 1)
						channel_info = line[0].strip().split('/', 1)

						if len(channel_info) == 2:
							source = channel_info[0]
							source_id = channel_info[1]
						else:
							source = self.FALLBACK_SOURCE
							source_id = channel_info[0]

						self._channels['{}/{}'.format(source, source_id)] = {
							'source': source,
							'source_id': source_id,
							'name': line[1].strip() if len(line) == 2 else None,
						}

					if type(self._channels) is not dict or len(self._channels.keys()) == 0:
						raise FileNotFoundError
			except FileNotFoundError as error:
				self.logger.warning('No channels found in {}'.format(os.path.join(self.config_dir, 'channels')), exc_info=error)
				sys.exit(0)
			except OSError as error:
				self.logger.critical('Unable to read channels file {}'.format(os.path.join(self.config_dir, 'channels')), exc_info=error)
				sys.exit(1)

		return self._channels

	def set_channels(self, channels):
		self._channels = channels

		try:
			with open(os.path.join(self.config_dir, 'channels'), 'w') as f:
				for channel_id, channel in self._channels.items():
					f.write('{}/{}'.format(channel['source'], channel['source_id']))

					if channel['name'] is not None:
						f.write(' # {}'.format(channel['name']))

					f.write('\n')
		except OSError as error:
			self.logger.critical('Unable to update {}'.format(os.path.join(self.config_dir, 'channels')), exc_info=error)
			sys.exit(1)

	_channels = None
	channels = property(get_channels, set_channels)

	# Cache
	def get_cache_dir(self):
		if self._cache_dir is None:
			self._cache_dir = self.config['cache_directory']

			try:
				os.makedirs(self._cache_dir, exist_ok=True)
			except OSError as error:
				self.logger.critical('Unable to create cache directory {}, aborting...'.format(self._cache_dir), exc_info=error)
				sys.exit(1)

			# Move any legacy files not in a source-named named directory
			files_to_move = set()

			for entry in os.scandir(self._cache_dir):
				if entry.is_file() and entry.name != 'next' and entry.name != '{}.log'.format(__name__):
					files_to_move.add(entry)

			if len(files_to_move) > 0:
				try:
					os.makedirs(os.path.join(self._cache_dir, self.FALLBACK_SOURCE), exist_ok=True)
				except OSError as error:
					self.logger.critical('Unable to create cache directory {}, aborting...'.format(os.path.join(self._cache_dir, self.FALLBACK_SOURCE)), exc_info=error)
					sys.exit(1)

				for file in files_to_move:
					try:
						shutil.move(file.path, os.path.join(self._cache_dir, self.FALLBACK_SOURCE, file.name))
					except OSError as error:
						self.logger.critical('Unable to move file {} to {}, aborting...'.format(file.path, os.path.join(self._cache_dir, self.FALLBACK_SOURCE)), exc_info=error)
						sys.exit(1)

		return self._cache_dir

	_cache_dir = None
	cache_dir = property(get_cache_dir)

	def get_next_channel_index(self):
		if self._next_channel_index is None:
			# Always start at the beginning when checking all channels
			if self.config['channels_per_run'] is None:
				self._next_channel_index = 0
			else:
				try:
					with open(os.path.join(self.cache_dir, 'next'), 'r') as f:
						next_channel = f.read().strip()

						if type(next_channel) is not str or next_channel == '':
							raise FileNotFoundError

						if '/' not in next_channel:
							next_channel = 'youtube/{}'.format(next_channel)

						if next_channel in self.channels:
							self._next_channel_index = list(self.channels).index(next_channel)
						else:
							raise FileNotFoundError
				except FileNotFoundError:
					self._next_channel_index = 0
				except OSError as error:
					self.logger.critical('Unable to read status file {}'.format(os.path.join(self.cache_dir, 'next')), exc_info=error)
					sys.exit(1)

		return self._next_channel_index

	def set_next_channel_index(self, channel_index):
		self._next_channel_index = channel_index

		try:
			with open(os.path.join(self.cache_dir, 'next'), 'w') as f:
				f.write('{}\n'.format(list(self.channels)[self._next_channel_index]))
		except OSError as error:
			self.logger.critical('Unable to update status file {}'.format(os.path.join(self.cache_dir, 'next')), exc_info=error)
			sys.exit(1)

	_next_channel_index = None
	next_channel_index = property(get_next_channel_index, set_next_channel_index)

	def get_video_cache(self, channel):
		try:
			with open(os.path.join(self.cache_dir, channel['source'], channel['source_id']), 'r') as f:
				return set([self.normalize_string(line) for line in f])
		except FileNotFoundError:
			return None
		except OSError as error:
			self.logger.critical('Unable to read cache file {}'.format(os.path.join(self.cache_dir, channel['source'], channel['source_id'])), exc_info=error)
			sys.exit(1)

	def add_to_video_cache(self, channel, video_ids):
		try:
			os.makedirs(os.path.join(self._cache_dir, channel['source']), exist_ok=True)
		except OSError as error:
			self.logger.critical('Unable to create cache directory {}, aborting...'.format(os.path.join(self._cache_dir, channel['source'])), exc_info=error)
			sys.exit(1)

		try:
			with open(os.path.join(self.cache_dir, channel['source'], channel['source_id']), 'a') as f:
				if type(video_ids) is str:
					video_ids = [video_ids]

				for video_id in video_ids:
					f.write('{}\n'.format(video_id))
		except OSError as error:
			self.logger.critical('Unable to update cache file {}'.format(os.path.join(self.cache_dir, channel['source'], channel['source_id'])), exc_info=error)
			sys.exit(1)

	# Video data retrieval logic

	def get_video_data(self, video_string, channel=None, publish_date=None):
		if channel is not None:
			if channel['source'] == 'youtube':
				return self.get_youtube_video_data(video_string, channel, publish_date)
			elif channel['source'] == 'nebula':
				return self.get_nebula_video_data(video_string, channel)
		else:
			source = None

			# Let's try basic URL checks first
			for source_name in self.SOURCES:
				for domain in self.SOURCES[source_name]['match_domains']:
					if '{}/'.format(domain) in video_string:
						source = source_name
						break

				if source is not None:
					break

			# YouTube IDs are always 11 characters long, so if we have that, it's very likely that it's a YT video,
			# as Nebula video slugs always contain the channel name
			if source is None and len(video_string) == 11:
				source = 'youtube'

			# Check if the URL is lowercase, as that's the case for Nebula video slugs
			if source is None and video_string.islower() is True:
				source = 'nebula'

			# We've ran out of ideas, so let's go with YT, as it's more popular
			if source is None:
				source = self.FALLBACK_SOURCE

			if source == 'youtube':
				return self.get_youtube_video_data(video_string, publish_date)
			elif source == 'nebula':
				return self.get_nebula_video_data(video_string)

	def get_youtube_video_data(self, video_string, channel=None, publish_date=None):
		video_data = {}

		try:
			with yt_dlp.YoutubeDL({
				'logger': self.ydl_logger,
				'noprogress': True,
				'no_color': True,
			}) as ydl:
				video_data = ydl.extract_info('{}'.format(video_string), download=False, ie_key='Youtube')

				if self.normalize_prop_string(video_data, 'extractor') != 'youtube':
					self.logger.warning('Video is not a {} video, skipping...'.format(self.SOURCES['youtube']['readable_name']))
					return None

				if 'is_live' in video_data and video_data['is_live'] is True:
					# example as of 2023/06/22 - jfKfPfyJRdk (tested in CZ)
					self.logger.warning('Video is currently live, skipping...')
					return None
		except yt_dlp.utils.DownloadError as error:
			error_string = str(error)

			if 'has not made this video available in your country' in error_string:
				# example as of 2024/05/21 - 83SA-kuG0G8 (tested in CZ)
				self.logger.warning('Video is not available in your country, skipping...')
				return None
			elif 'who has blocked it on copyright grounds' in error_string:
				# example as of 2024/05/21 - fshgBDgtfRg (tested in CZ)
				self.logger.warning('Video is blocked on copyright grounds, skipping...')
				return None
			elif 'live event will begin' in error_string:
				# no long living example available
				self.logger.warning('Video is a future live stream, skipping...')
				return None
			elif ': Offline.' in error_string:
				# no long living example available
				self.logger.warning('Live stream is offline, skipping...')
				return None
			elif 'live stream recording is not available' in error_string:
				# no long living example available
				self.logger.warning('Video is an unavailable live stream recording, skipping...')
				return None
			elif 'Premieres in' in error_string or 'Premiere will begin shortly' in error_string:
				# no long living example available
				self.logger.warning('Video is a future premiere, skipping...')
				return None
			elif 'to get access to members-only content' in error_string:
				# no long living example available
				self.logger.warning('Video is only available to members, skipping...')
				return None
			elif 'Private video' in error_string:
				# example as of 2024/05/21 - j9j6SN9Y4cE
				self.logger.warning('Video is private, skipping...')
				return None
			elif 'account associated with this video has been terminated' in error_string:
				# example as of 2024/05/21 - jTZM8zGSfko
				self.logger.warning('Video was uploaded on a terminated account, skipping...')
				return None
			elif 'Sign in to confirm your age' in error_string:
				# no long living example available
				self.logger.warning('Video is age-gated and yt-dlp was not able to download it, skipping...')
				return None
			elif 'Video unavailable' in error_string or 'The following content is not available on this app' in error_string:
				# examples as of 2024/05/21 - 2Fgl4jJJOcM, Wdluq-av2nU
				self.logger.warning('Video unavailable, skipping...')
				return None
			else:
				self.logger.error('Unable to download information for video {} on {}'.format(video_string, self.SOURCES['youtube']['readable_name']), exc_info=error)
				raise RuntimeError
		except TimeoutError as error:
			self.logger.error('Unable to download information for video {} on {}'.format(video_string, self.SOURCES['youtube']['readable_name']), exc_info=error)
			raise RuntimeError

		data = {
			'id': self.normalize_prop_string(video_data, 'id'),
			'title': self.normalize_prop_string(video_data, 'title'),
			'published': publish_date if publish_date is not None else datetime.datetime.strptime(self.normalize_prop_string(video_data, 'upload_date'), '%Y%m%d').strftime('%Y-%m-%d'),
			'description': self.normalize_prop_string(video_data, 'description', False),
		}

		if channel is not None:
			data['channel'] = channel
		else:
			data['channel'] = {
				'source': 'youtube',
				'source_id': self.normalize_prop_string(video_data, 'channel_id'),
				'name': self.normalize_prop_string(video_data, 'uploader'),
			}

		return data

	def get_nebula_video_data(self, video_string, channel=None):
		json_string = None
		video_data = None

		# Get video info
		for domain in self.SOURCES['nebula']['match_domains']:
			video_string = video_string.replace('https://{}/videos/'.format(domain), '')

		video_string = video_string.rstrip('/')

		url = '{}{}/'.format(self.SOURCES['nebula']['api_base_url'], urllib.parse.quote(video_string))
		self.logger.debug('Sending a GET request to {}'.format(url))

		try:
			json_string = urllib.request.urlopen(urllib.request.Request(url, headers={'User-Agent': self.USER_AGENT})).read().decode('utf-8')
		except (http.client.HTTPException, TimeoutError, urllib.error.URLError) as error:
			self.logger.error('Unable to download information for video {} on {}'.format(video_string, self.SOURCES['nebula']['readable_name']), exc_info=error)
			raise RuntimeError

		try:
			video_data = json.loads(json_string)
		except json.JSONDecodeError as error:
			self.logger.error('Unable to parse information for video {} on {}'.format(video_string, self.SOURCES['nebula']['readable_name']), exc_info=error)
			raise RuntimeError

		data = {
			'id': self.normalize_prop_string(video_data, 'slug'),
			'title': self.normalize_prop_string(video_data, 'title'),
			'published': datetime.datetime.fromisoformat(self.normalize_prop_string(video_data, 'published_at', False).replace('Z', '+00:00')).strftime('%Y-%m-%d %H-%M'),
			'description': self.normalize_prop_string(video_data, 'description', False),
		}

		if channel is not None:
			data['channel'] = channel
		else:
			data['channel'] = {
				'source': 'nebula',
				'source_id': self.normalize_prop_string(video_data, 'channel_slug'),
				'name': self.normalize_prop_string(video_data, 'channel_title'),
			}

		return data

	# Channel data retrieval logic

	def get_channel_data(self, channel):
		if channel['source'] == 'youtube':
			return self.get_youtube_channel_data(channel)
		elif channel['source'] == 'nebula':
			return self.get_nebula_channel_data(channel)

	rss_namespaces_youtube = None

	def get_youtube_channel_data(self, channel):
		rss_string = None
		raw_data = None
		all_videos = {}

		data = {
			'name': None,
			'videos': set(),
			'published': {},
		}

		# Get channel info
		url = '{}{}'.format(self.SOURCES['youtube']['api_base_url'], urllib.parse.quote(channel['source_id']))
		self.logger.debug('Sending a GET request to {}'.format(url))

		try:
			rss_string = urllib.request.urlopen(urllib.request.Request(url, headers={'User-Agent': self.USER_AGENT})).read().decode('utf-8')
		except (http.client.HTTPException, TimeoutError, urllib.error.URLError) as error:
			self.logger.error('Unable to download information for channel {} on {}'.format(channel['source_id'], self.SOURCES['youtube']['readable_name']), exc_info=error)
			raise RuntimeError

		try:
			raw_data = xml.etree.ElementTree.fromstring(rss_string)
		except xml.etree.ElementTree.ParseError as error:
			self.logger.error('Unable to parse information for channel {} on {}'.format(channel['source_id'], self.SOURCES['youtube']['readable_name']), exc_info=error)
			raise RuntimeError

		if self.rss_namespaces_youtube is None:
			self.rss_namespaces_youtube = dict([
				node for _, node in xml.etree.ElementTree.iterparse(
					io.StringIO(rss_string), events=['start-ns']
				)
			])

		if (
			(channel_name := raw_data.find('./author/name', self.rss_namespaces_youtube)) is not None and
			(channel_name := self.normalize_string(channel_name.text)) is not None
		):
			data['name'] = channel_name
		else:
			self.logger.error('Unable to get channel name for channel {} on {}'.format(channel['source_id'], self.SOURCES['youtube']['readable_name']))
			raise RuntimeError

		# Make sure we always have a list for the main channel, as we always want to have a cache file for it
		all_videos[channel['source_id']] = set()

		# Figure out which videos we want to download
		for video_id in raw_data.findall('./entry/yt:videoId', self.rss_namespaces_youtube):
			if (
				(video_id := self.normalize_string(video_id.text)) is not None and
				(subchannel_id := raw_data.find("./entry/yt:videoId[.='{}']/../yt:channelId".format(video_id), self.rss_namespaces_youtube)) is not None and
				(subchannel_id := self.normalize_string(subchannel_id.text)) is not None
			):
				# Some (artist?) channels have videos from multiple subchannels included in them (e.g. https://www.youtube.com/EmmaBlackery)
				if subchannel_id not in all_videos:
					all_videos[subchannel_id] = set()

				all_videos[subchannel_id].add(video_id)

		main_channel_cache_empty = self.get_video_cache(channel) is None

		for subchannel_id in all_videos:
			subchannel_videos = all_videos[subchannel_id]

			if len(subchannel_videos) != 0:
				videos_cache = self.get_video_cache({
					'source': channel['source'],
					'source_id': subchannel_id,
				})

				# Always checking the main channel cache allows us to download videos from newly spotted subchannels
				if main_channel_cache_empty is False:
					data['videos'] = data['videos'].union(subchannel_videos.difference(videos_cache))
				else:
					self.add_to_video_cache({
						'source': channel['source'],
						'source_id': subchannel_id,
					}, subchannel_videos)
			else:
				# Make sure the cache file for the subchannel exists by adding an empty list to the cache
				self.add_to_video_cache({
					'source': channel['source'],
					'source_id': subchannel_id,
				}, [])

		# Get publish date information
		for video_id in data['videos']:
			if (
				(video_published := raw_data.find("./entry/yt:videoId[.='{}']/../published".format(video_id), self.rss_namespaces_youtube)) is not None and
				(video_published := self.normalize_string(video_published.text)) is not None
			):
				data['published'][video_id] = datetime.datetime.fromisoformat(video_published).strftime('%Y-%m-%d %H-%M')
			else:
				data['published'][video_id] = None
				self.logger.warning('Unable to get video publish time for video {} on {}'.format(video_id, self.SOURCES['youtube']['readable_name']))

		return data

	def get_nebula_channel_data(self, channel):
		json_string = None
		raw_data = None
		all_videos = set()

		data = {
			'name': None,
			'videos': set(),
		}

		# Get channel info
		url = '{}channels/{}/'.format(self.SOURCES['nebula']['api_base_url'], urllib.parse.quote(channel['source_id']))
		self.logger.debug('Sending a GET request to {}'.format(url))

		try:
			json_string = urllib.request.urlopen(urllib.request.Request(url, headers={'User-Agent': self.USER_AGENT})).read().decode('utf-8')
		except (http.client.HTTPException, TimeoutError, urllib.error.URLError) as error:
			self.logger.error('Unable to download information for channel {} on {}'.format(channel['source_id'], self.SOURCES['nebula']['readable_name']), exc_info=error)
			raise RuntimeError

		try:
			raw_data = json.loads(json_string)
		except json.JSONDecodeError as error:
			self.logger.error('Unable to parse information for channel {} on {}'.format(channel['source_id'], self.SOURCES['nebula']['readable_name']), exc_info=error)
			raise RuntimeError

		for prop in ['details', 'episodes']:
			if prop not in raw_data:
				self.logger.error('Required object {} not present'.format(prop))
				raise RuntimeError

		if 'results' not in raw_data['episodes']:
			self.logger.error('Required array episodes.results not present')
			raise RuntimeError

		data['name'] = self.normalize_prop_string(raw_data['details'], 'title')

		# Figure out which videos we want to download
		for video in raw_data['episodes']['results']:
			if (video_id := self.normalize_string(video['slug'])) is not None:
				all_videos.add(video_id)

		if len(all_videos) != 0:
			videos_cache = self.get_video_cache(channel)

			if videos_cache is not None:
				data['videos'] = data['videos'].union(all_videos.difference(videos_cache))
			else:
				self.add_to_video_cache(channel, all_videos)
		else:
			# Make sure the cache file for the channel exists by adding an empty list to the cache
			self.add_to_video_cache(channel, [])

		return data

	# Video data processing logic

	def download_video(self, video_data, save_to_cache=True, additional_id=None):
		ignore_video = False

		# Try to match each of our ignore_videos regexes if we're not working with a one-off video
		if save_to_cache is True:
			for regex in self.config['ignore_videos']:
				if regex.search(video_data['title']) is not None:
					ignore_video = True
					break

		log_output = ' video {} from {} on {}'.format(video_data['title'], video_data['channel']['name'], self.SOURCES[video_data['channel']['source']]['readable_name'])

		if ignore_video is False:
			# If no regex matches or we're working with a one-off video, download the video as usual
			self.logger.info('Downloading{}'.format(log_output))

			dir_name = self.sanitize_filename('{} - {} - {}'.format(video_data['published'], video_data['channel']['name'], video_data['title']))
			dest_dir = os.path.join(self.video_dir, dir_name)

			try:
				os.makedirs(dest_dir, exist_ok=True)
			except OSError as error:
				self.logger.critical('Unable to create directory {}, aborting...'.format(dest_dir), exc_info=error)
				sys.exit(1)

			if self.config['description'] is True and video_data['description'] is not None:
				try:
					with open(os.path.join(dest_dir, 'description.txt'), 'w') as f:
						f.write('{}\n'.format(video_data['description']))
				except OSError as error:
					self.logger.critical('Unable to create description file {}, aborting...'.format(os.path.join(dest_dir, 'description.txt')), exc_info=error)
					sys.exit(1)

			if video_data['channel']['source'] == 'youtube':
				self.download_youtube_video(video_data, dest_dir, dir_name)
			elif video_data['channel']['source'] == 'nebula':
				self.download_nebula_video(video_data, dest_dir, dir_name)
		else:
			# If any of the regexes match and we're not working with a one-off video, don't download the video
			self.logger.info('Ignoring{}'.format(log_output))

		# save_to_cache is set to False for one-off videos to prevent all videos being downloaded when the channel is later added to the channel list
		if save_to_cache is True:
			self.add_to_video_cache(video_data['channel'], video_data['id'])

			if additional_id is not None:
				self.add_to_video_cache(video_data['channel'], additional_id)

	def download_youtube_video(self, video_data, destination, directory_name):
		try:
			with yt_dlp.YoutubeDL({
				'outtmpl': os.path.join(destination.replace('%', '%%'), '{}.%(ext)s'.format(directory_name.replace('%', '%%'))),
				**({'format_sort': ['res:{}'.format(self.config['max_resolution']), '+size']} if self.config['max_resolution'] is not None else {}),
				'writesubtitles': self.config['subtitles'],
				**({'subtitleslangs': ['all', '-live_chat']} if self.config['subtitles'] is True else {}),
				'logger': self.ydl_logger,
				'noprogress': True,
				'no_color': True,
			}) as ydl:
				ydl.download(['{}{}'.format(self.SOURCES['youtube']['video_base_url'], video_data['id'])])
		except (yt_dlp.utils.DownloadError, TimeoutError) as error:
			self.logger.error('Unable to download video {} from {} on {}'.format(video_data['title'], video_data['channel']['name'], self.SOURCES['youtube']['readable_name']), exc_info=error)
			raise RuntimeError

	def download_nebula_video(self, video_data, destination, directory_name):
		# No way to anonymously download Nebula videos, so let's just create a shortcut instead
		url = '{}{}'.format(self.SOURCES['nebula']['video_base_url'], video_data['id'])
		extension = None
		content = None

		if sys.platform.startswith('linux'):
			# Use .desktop file format for Linux
			# https://specifications.freedesktop.org/desktop-entry-spec/latest/
			extension = 'desktop'
			content = [
				'[Desktop Entry]',
				'Version=1.0',
				'Type=Link',
				'Name={}'.format(directory_name),
				'Comment=',
				'Icon=internet-web-browser',
				'URL={}'.format(url),
			]
		elif sys.platform.startswith('win32') or sys.platform.startswith('darwin'):
			# Use .url file format for Windows and macOS
			# http://www.lyberty.com/encyc/articles/tech/dot_url_format_-_an_unofficial_guide.html
			extension = 'url'
			content = [
				'[InternetShortcut]',
				'URL={}'.format(url),
				'IconIndex=0',
			]
		else:
			# Create a text file with the URL inside for any other OS
			extension = 'txt'
			content = [
				'{}'.format(url),
			]

		shortcut_path = os.path.join(destination, '{}.{}'.format(directory_name, extension))

		try:
			with open(shortcut_path, 'w') as f:
				f.write('{}\n'.format('\n'.join(content)))

			# As far as I can tell, setting +x is only required for .desktop files
			if sys.platform.startswith('linux'):
				os.chmod(shortcut_path, os.stat(shortcut_path).st_mode | stat.S_IEXEC)
		except OSError as error:
			self.logger.critical('Unable to create shortcut file {}, aborting...'.format(shortcut_path), exc_info=error)
			raise RuntimeError

	# Channel data processing logic
	def process_channel(self, channel_id):
		channel = self.channels[channel_id]
		source = self.SOURCES[channel['source']]

		# Get the channel data
		data = self.retry(self.get_channel_data, channel, retry_count=self.config['retries'])

		if data is False or data is None:
			return None

		self.logger.info('Checking channel {} on {} for new videos'.format(data['name'], source['readable_name']))

		# Re-set self.channels to save channel name to file
		channels = self.channels
		channels[channel_id]['name'] = data['name']
		self.channels = channels

		for video_id in data['videos']:
			# YouTube video pages themselves only provide a publish date, so we use the timestamp from the channel RSS
			publish_date = None
			if channel['source'] == 'youtube':
				publish_date = data['published'][video_id]

			# Get information about the video
			video_data = self.retry(self.get_video_data, video_id, channel=channel, publish_date=publish_date, retry_count=self.config['retries'])

			if video_data is False or video_data is None:
				continue

			# YouTube Premium videos (e.g. https://www.youtube.com/watch?v=k_vD2S49INE) return a different video than requested (a trailer),
			# so we compare the requested ID with the data we got back and if it's different, we add the requested ID as an additional ID
			# to be cached. We can compare directly without going through ytdl's video string parser, as we get a valid ID directly from YouTube.
			additional_id = None
			if channel['source'] == 'youtube' and video_id != video_data['id']:
				additional_id = video_id

			# Download information about the video
			self.retry(self.download_video, video_data, additional_id=additional_id, retry_count=self.config['retries'])

def cli():
	# Handle SIGINT
	signal.signal(signal.SIGINT, lambda *args, **kwargs: sys.exit(0))

	# Configure CLI arguments
	parser = argparse.ArgumentParser(
		formatter_class=argparse.RawTextHelpFormatter,
		description='\n'.join([
			'description:',
			'  {prog} is a video download tool that automatically downloads newly posted',
			'  videos from channels you specify on supported video hosting websites.\n',
			'  Although you can run it manually, the recommended way is to set up a systemd',
			'  timer or a cron to run this every X minutes.\n',
			'supported websites:',
			'  YouTube - identifier: youtube',
			'  Nebula - identifier: nebula',
			'    only supports creating shortcuts with an URL\n',
			'configuration:',
			'  You\'ll want to pay attention to two files, both located in the config',
			'  directory that is located in $XDG_CONFIG_HOME/{prog} by default, with',
			'  $APPDATA/{prog} and ~/.config/{prog} as fallbacks unless overriden',
			'  by the config_dir argument:\n',
			'  CONFIG_DIR/config:',
			'    Config file for the tool, the values provided here are the defaults used',
			'    when a property is not present or when the config file doesn\'t exist, unless',
			'    specified otherwise.\n',
			'    # cache directory for the status file and channel-specific cache files',
			'    cache_directory = ~/.cache/{prog}',
			'    # directory for downloaded videos',
			'    video_directory = ~/{prog}',
			'    # maximum number of retries for retrieving channels and videos',
			'    retries = 5',
			'    # amount of channels to check per run, all if not a number bigger than zero,',
			'    # can be used if you\'re getting rate limited by websites',
			'    channels_per_run = all',
			'    # multiline string, with each line containing a regex to match against video',
			'    # titles (not checked for specifically requested videos), in case of a case',
			'    # insensitive match, the video is not downloaded, the regexes provided here',
			'    # are examples, none are specified by default',
			'    ignore_videos =',
			'        ABC Show',
			'        ^XYZ Podcast - Episode \\d+$',
			'    # maximum size of the smallest dimension of the downloaded video, an equal',
			'    # or the first smaller resolution is downloaded, if there are multiple',
			'    # versions with the same resolution, the version with the smallest filesize',
			'    # is used, the size provided here is an example, no limit is applied',
			'    # by default',
			'    max_resolution = 480',
			'    # download the video\'s description, placed in the video subdirectory if true',
			'    description = true',
			'    # download the video\'s subtitles, placed in the video subdirectory if true',
			'    subtitles = true\n',
			'  CONFIG_DIR/channels:',
			'    A newline separated list of channel IDs wih website identifiers to check',
			'    for new videos. If no website identifier is present, YouTube is assumed.',
			'    Channel names are automatically added as comments when the channel',
			'    is checked. The file below is an example.\n',
			'    youtube/UCK8sQmJBp8GCxrOtXWBpyEA # Google',
			'    UCPPZoYsfoSekIpLcz9plX1Q',
			'    nebula/secondthought # Second Thought',
		]).format(prog=__name__),
		prog=__name__
	)

	parser.add_argument(
		'video',
		nargs='*',
		type=str,
		metavar='VIDEO',
		help='\n'.join([
			'one or more specific videos to download instead',
			'of checking channels, represented by an ID or a URL',
		])
	).completer = argcomplete.completers.SuppressCompleter()

	parser.add_argument(
		'--version',
		action='version',
		version=__version__
	)

	parser.add_argument(
		'-c', '--config_dir',
		type=str,
		help='set a path to use as a directory for the config files'
	).completer = argcomplete.completers.DirectoriesCompleter()

	parser.add_argument(
		'-v', '--verbose',
		action='store_true',
		help='print debug output'
	)

	# Implement shell tab completion
	argcomplete.autocomplete(parser)

	# Parse CLI arguments
	args = parser.parse_args()

	# Set up the base logger
	kahlo_logger = KahloLogger(verbose=args.verbose)

	# Create the main kahlo instance
	kahlo = Kahlo(logger=kahlo_logger)

	# If we got a different config directory via an argument, pass it to the main kahlo instance
	if args.config_dir is not None:
		kahlo.config_dir = args.config_dir

	# Add the file logging afterwards, because the main kahlo instance must exist and must have
	# the correct config directory set for us to be able to get a path to the cache directory
	file_handler = logging.FileHandler(os.path.join(kahlo.cache_dir, '{}.log'.format(__name__)))
	file_handler.setLevel(logging.DEBUG)
	file_handler.setFormatter(logging.Formatter(fmt='[%(asctime)s] [%(levelname)s] %(message)s', datefmt='%Y-%m-%dT%H:%M:%S%z'))
	kahlo_logger.logger.addHandler(file_handler)

	# Handle the CLI logic
	if len(args.video) > 0:
		# The user specified specific videos to download, loop through them
		for video_string in args.video:
			# Get the video data and check if we got it
			video_data = kahlo.retry(kahlo.get_video_data, video_string, retry_count=kahlo.config['retries'])

			if video_data is False or video_data is None:
				continue

			# If we have the data, download the video
			kahlo.retry(kahlo.download_video, video_data, save_to_cache=False, retry_count=kahlo.config['retries'])
	else:
		# The user wants to check their channels, figure out how many we want to process
		left_to_process = len(kahlo.channels) if kahlo.config['channels_per_run'] is None else kahlo.config['channels_per_run']

		# Process each channel and prepare the index of the next channel
		while left_to_process > 0:
			kahlo.process_channel(list(kahlo.channels)[kahlo.next_channel_index])
			kahlo.next_channel_index = kahlo.next_channel_index + 1 if kahlo.next_channel_index + 1 < len(kahlo.channels) else 0
			left_to_process -= 1
