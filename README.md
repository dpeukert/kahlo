# kahlo

`kahlo` is a video download tool that automatically downloads newly posted videos from channels you specify on supported video hosting websites.

Please note that I haven't tested this tool on macOS or Windows.

## Supported websites
* YouTube - doesn't use the official authenticated YouTube API provided by Google
* Nebula - only creates shortcuts with an URL

## How do I use this?
```
usage: kahlo [-h] [--version] [-c CONFIG_DIR] [-v] [VIDEO ...]

description:
  kahlo is a video download tool that automatically downloads newly posted
  videos from channels you specify on supported video hosting websites.

  Although you can run it manually, the recommended way is to set up a systemd
  timer or a cron to run this every X minutes.

supported websites:
  YouTube - identifier: youtube
  Nebula - identifier: nebula
    only supports creating shortcuts with an URL

configuration:
  You'll want to pay attention to two files, both located in the config
  directory that is located in $XDG_CONFIG_HOME/kahlo by default, with
  $APPDATA/kahlo and ~/.config/kahlo as fallbacks unless overriden
  by the config_dir argument:

  CONFIG_DIR/config:
    Config file for the tool, the values provided here are the defaults used
    when a property is not present or when the config file doesn't exist, unless
    specified otherwise.

    # cache directory for the status file and channel-specific cache files
    cache_directory = ~/.cache/kahlo
    # directory for downloaded videos
    video_directory = ~/kahlo
    # maximum number of retries for retrieving channels and videos
    retries = 5
    # amount of channels to check per run, all if not a number bigger than zero,
    # can be used if you're getting rate limited by websites
    channels_per_run = all
    # multiline string, with each line containing a regex to match against video
    # titles (not checked for specifically requested videos), in case of a case
    # insensitive match, the video is not downloaded, the regexes provided here
    # are examples, none are specified by default
    ignore_videos =
        ABC Show
        ^XYZ Podcast - Episode \d+$
    # maximum size of the smallest dimension of the downloaded video, an equal
    # or the first smaller resolution is downloaded, if there are multiple
    # versions with the same resolution, the version with the smallest filesize
    # is used, the size provided here is an example, no limit is applied
    # by default
    max_resolution = 480
    # download the video's description, placed in the video subdirectory if true
    description = true
    # download the video's subtitles, placed in the video subdirectory if true
    subtitles = true

  CONFIG_DIR/channels:
    A newline separated list of channel IDs wih website identifiers to check
    for new videos. If no website identifier is present, YouTube is assumed.
    Channel names are automatically added as comments when the channel
    is checked. The file below is an example.

    youtube/UCK8sQmJBp8GCxrOtXWBpyEA # Google
    UCPPZoYsfoSekIpLcz9plX1Q
    nebula/secondthought # Second Thought

positional arguments:
  VIDEO                 one or more specific videos to download instead
                        of checking channels, represented by an ID or a URL

options:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -c CONFIG_DIR, --config_dir CONFIG_DIR
                        set a path to use as a directory for the config files
  -v, --verbose         print debug output
```

## Cool, how do I go about installing this thing?

- use the [kahlo AUR package](https://aur.archlinux.org/packages/kahlo/)
- at some point, I'll upload this to PyPi
- download this repo and build it manually
  - fulfill the following dependencies
    - `python>=3.8.0`
    - `yt-dlp`
    - `python-argcomplete`
    - `python-build`
    - `python-installer` or `python-pip`
  - `python -m build`
  - `python -m installer --destdir="$destdir" dist/*.whl` or `pip install dist/*.whl`

## Why the name?

`kahlo` is named after [Frida Kahlo](https://en.wikipedia.org/wiki/Frida_Kahlo), a Mexican painter and communist known for her many paintings inspired by Mexican folk art and mixing elements from the precolonial, colonial and postcolonial eras alike. Her paintings explored the roles of identity, class, race and gender in and the influence of colonialism on Mexican society.

## License
The contents of this repository are provided under the GPLv3 license or any later version (SPDX identifier: `GPL-3.0-or-later`).
